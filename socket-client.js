function GamewheelSocketClient(__url, __io) {

    return {

        url: __url,
        
        io : __io,

        socket: null,

        onConnect: null,

        onEvent: null,

        onDisconnect: null,
        
        events : [],
        
        onJoinSuccess : null,

        onJoinFailure : null,

        configuration : null,

        sync : false,

        syncFrequency : 250,

        latency : 0,

        join : function(__roomName) {
            
            if (this.socket) {
                this.socket.emit('join', {roomName : __roomName});
                return;
            }
            
            console.log('not connected');
        },
        
        addEvent : function(__eventName, __callback){
            this.events.push({name : __eventName, callback : __callback});
        },
        
        send: function (__action, __data) {

            if (this.socket) {
                this.socket.emit(__action, __data);
                return;
            }
            
            console.log('not connected');
        },

        start: function(__roomName) {
            console.log('request to start game for room ' + __roomName);
            this.send(
                'start',
                {
                    roomName : __roomName
                }
            );
        },

        synchronize : function () {
            this.send(
                'sync',
                {
                    time: Date.now()
                }
            );
            window.setTimeout(this.synchronize, this.syncFrequency);
        },
        
        connect: function (sessionId) {

            if (this.socket) {
                this.disconnect();
            }

            var url = this.url;

            if (sessionId) {
                url += '/' + sessionId;
            }

            this.socket = this.io(
                url,
                {
                    reconnection : false
                }
            );

            this.socket.on(
                'connect',
                function () {
                    if (this.onConnect) {
                        this.onConnect(this.socket);
                    }
                }.bind(this)
            );
            
            for (var i = 0; i < this.events.length; i++) {
                this.socket.on(
                    this.events[i].name,
                    this.events[i].callback
                );
            }

            this.socket.on(
                'join',
                function(data) {
                    if (data.result === 'success') {
                        if (this.onJoinSuccess) {
                            this.onJoinSuccess(data);
                        }
                    } else {
                        if (this.onJoinFailure) {
                            this.onJoinFailure(data);
                        }
                    }
                }.bind(this)
            );

            this.socket.on(
                'sync',
                function (data) {
                    var currentTime = Date.now();
                    this.latency = (currentTime - data.time) / 2;
                }.bind(this)
            );

            this.socket.on(
                'disconnect',
                function() {

                    console.log('client disconnect');

                    if (this.onDisconnect) {
                        this.onDisconnect();
                    }

                    this.socket = null;
                }
            )
        },

        disconnect: function () {

            if (this.socket) {
                this.socket.disconnect();
                return;
            }

            console.log("not connected");
        }
    };
}
if (typeof module !== 'undefined') {
    module.exports.socketClient = GamewheelSocketClient;
}