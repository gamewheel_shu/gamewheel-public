module.exports = {
    'socketClient' : function(__url, __io) {
        return require('./socket-client.js').socketClient(__url, __io);
    },
    'threeClient' : function(__three) {
        return require('./three-client.js').threeClient(__three);
    }
};