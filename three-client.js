function GamewheelThreeClient(__three) {

    var renderer;

    function createDisplay(
        __width,
        __height
    ) {
        renderer = new __three.WebGLRenderer();
        renderer.setSize(__width, __height);
        document.body.appendChild(renderer.domElement);
    }

    return {
        createDisplay : createDisplay
    };

}

if (typeof module !== 'undefined') {
    module.exports.threeClient = GamewheelThreeClient;
}